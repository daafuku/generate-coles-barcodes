package main

import (
	"flag"
	"fmt"
	"image/png"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code128"
)

var (
	addr string
)

func main() {
	flag.StringVar(&addr, "l", "127.0.0.1:8888", "server listen address")

	server := http.Server{
		Addr: addr,
		Handler: http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			if req.URL.Path != "/" {
				http.NotFound(res, req)
				return
			}

			var addDays int = 1

			{
				dayString := req.URL.Query().Get("day")
				if dayString != "" {
					if day, err := strconv.ParseInt(dayString, 10, 8); err != nil {
						log.Println(fmt.Errorf("failed to parse day: %w", err))
						res.WriteHeader(http.StatusBadRequest)
						return
					} else {
						addDays = int(day)
					}
				}
			}

			expires := time.Now()
			expires = time.Date(
				expires.Year(),
				expires.Month(),
				expires.Day(),
				0, 0, 0, 0,
				expires.Location())

			expires = expires.AddDate(0, 0, addDays)

			text := fmt.Sprintf("88%s", expires.Format("060102"))

			code, err := code128.Encode(text)
			if err != nil {
				log.Println(fmt.Errorf("failed to encode barcode: %w", err))
				res.WriteHeader(http.StatusInternalServerError)
				return
			}

			scaledCode, err := barcode.Scale(code, 1024, 512)
			if err != nil {
				log.Println(fmt.Errorf("failed to scale barcode: %w", err))
				res.WriteHeader(http.StatusInternalServerError)
				return
			}

			res.Header().Add("Content-Type", "image/png")
			res.Header().Add("Expires", expires.In(time.UTC).Format(http.TimeFormat))

			res.WriteHeader(http.StatusOK)
			err = png.Encode(res, scaledCode)
			if err != nil {
				log.Println(fmt.Errorf("failed to write barcode: %w", err))
				return
			}
		}),
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
