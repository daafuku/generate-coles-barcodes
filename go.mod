module gitlab.com/daafuku/generate-coles-barcodes

go 1.17

require github.com/boombuler/barcode v1.0.1

replace github.com/boombuler/barcode => ../../../github.com/darfk/barcode
